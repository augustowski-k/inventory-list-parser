﻿using InventoryListParser.Parser;
using NUnit.Framework;
using System;
using System.Linq;
using System.Text;

namespace InventoryListParser.Tests
{
    [TestFixture]
    public class InventoryCsvParserTests
    {
        [TestCase("Cherry Hardwood Arched Door - PS;COM-100001;WH-A,5|WH-B,10")]
        [TestCase("Cherry Hardwood Arched Door - PS;COM-100001;WH-A,5")]
        public void Parse_ValidLine_ReturnsListWithProperCount(string line)
        {
            InventoryCsvParser parser = new InventoryCsvParser();

            var result = parser.Parse(line);

            Assert.That(result.Count(), Is.EqualTo(1));
        }

        [TestCase(
            new[] { 
                "Cherry Hardwood Arched Door - PS;COM-100001;WH-A,5",
                "Cherry Hardwood Arched Door - PS;COM-100001;WH-A,5|WH-B,10" 
            }, 
            2)]
        [TestCase(
            new[] { 
                "Cherry Hardwood Arched Door - PS;COM-100001;WH-A,5",
                "Cherry Hardwood Arched Door - PS;COM-100001;WH-A,5|WH-B,10",
                "Cherry Hardwood Arched Door - PS;COM-100001;WH-A,5|WH-B,10" 
            }, 
            3)]
        [TestCase(
            new[] { 
                "# Cherry Hardwood Arched Door - PS;COM-100001;WH-A,5",
                "Cherry Hardwood Arched Door - PS;COM-100001;WH-A,5|WH-B,10",
                "Cherry Hardwood Arched Door - PS;COM-100001;WH-A,5|WH-B,10" 
            }, 
            2)]
        public void Parse_ValidMultiline_ReturnsListWithProperCount(string[] lines, int targetCount)
        {
            StringBuilder stringBuilder = new StringBuilder();
            foreach(string line in lines)
            {
                stringBuilder.AppendLine(line);
            }

            InventoryCsvParser parser = new InventoryCsvParser();

            var result = parser.Parse(stringBuilder.ToString());

            Assert.That(result.Count(), Is.EqualTo(targetCount));

        }

        [Test]
        public void Parse_CommentLine_ReturnsEmptyList()
        {
            string line = "# The comment";
            InventoryCsvParser parser = new InventoryCsvParser();

            var result = parser.Parse(line);

            Assert.That(result, Is.Empty);
        }
    }
}
