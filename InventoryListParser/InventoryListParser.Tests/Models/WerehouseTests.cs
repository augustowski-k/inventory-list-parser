﻿using InventoryListParser.Models;
using NUnit.Framework;
using System.Linq;

namespace InventoryListParser.Tests.Models
{
    [TestFixture]
    public class WerehouseTests
    {
        [Test]
        public void ItemCount_NoItems_ReturnsZero()
        {
            Werehouse werehouse = new Werehouse("name");

            int result = werehouse.ItemCount;

            Assert.That(result, Is.EqualTo(0));
        }

        [Test]
        public void ItemCount_ItemsPresent_ReturnsValidCount()
        {
            Werehouse werehouse = new Werehouse("name");
            werehouse.AddItem(new InventoryItem("item1", 5));
            werehouse.AddItem(new InventoryItem("item2", 2));
            werehouse.AddItem(new InventoryItem("item3", 3));

            int result = werehouse.ItemCount;

            Assert.That(result, Is.EqualTo(10));
        }

        [Test]
        public void AddItem_ValidItem_ProperCount()
        {
            Werehouse werehouse = new Werehouse("name");

            werehouse.AddItem(new InventoryItem("item1", 5));
            werehouse.AddItem(new InventoryItem("item2", 2));

            Assert.That(werehouse.Items.Count(), Is.EqualTo(2));
        }
    }
}
