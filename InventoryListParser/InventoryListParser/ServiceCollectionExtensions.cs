﻿using InventoryListParser.Parser;
using Microsoft.Extensions.DependencyInjection;

namespace InventoryListParser
{
    public static class ServiceCollectionExtensions
    {
        public static void AddInventoryListParser(this IServiceCollection services)
        {
            services.AddSingleton<IInventoryBinder, InventoryBinder>();
            services.AddSingleton<IInventoryReader, InventoryReader>();
            services.AddSingleton<IInventoryCsvParser, InventoryCsvParser>();
        }
    }
}
