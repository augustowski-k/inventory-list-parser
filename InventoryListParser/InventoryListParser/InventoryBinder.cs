﻿using InventoryListParser.Models;
using InventoryListParser.Parser;
using System.Collections.Generic;
using System.Linq;

namespace InventoryListParser
{
    public class InventoryBinder : IInventoryBinder
    {
        public Inventory Bind(IEnumerable<ParsedInventoryLine> parsedInventoryLines)
        {
            Dictionary<string, Werehouse> werehousedById = new Dictionary<string, Werehouse>();

            foreach(ParsedInventoryLine line in parsedInventoryLines)
            {
                string inventoryItemId = line.ItemId;

                foreach (var werehouseCount in line.CountByWerehouse)
                {
                    InventoryItem item = new InventoryItem(inventoryItemId, werehouseCount.Value);
                    if (werehousedById.ContainsKey(werehouseCount.Key))
                    {
                        werehousedById[werehouseCount.Key].AddItem(item);
                    }
                    else
                    {
                        Werehouse werehouse = new Werehouse(werehouseCount.Key);
                        werehouse.AddItem(item);
                        werehousedById[werehouseCount.Key] = werehouse;
                    }
                }
            }

            return new Inventory(werehousedById.Values.ToList());
        }
    }
}
