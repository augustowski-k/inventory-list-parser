﻿using System.Collections.Generic;
using System.IO;
using System.Text;

namespace InventoryListParser.Parser
{
    public interface IInventoryCsvParser
    {
        IEnumerable<ParsedInventoryLine> Parse(string csv);

        IEnumerable<ParsedInventoryLine> Parse(Stream csvStream);

        IEnumerable<ParsedInventoryLine> Parse(Stream csvStream, Encoding encoding);
    }
}