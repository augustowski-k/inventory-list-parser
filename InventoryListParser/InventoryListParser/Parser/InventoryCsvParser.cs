﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using TinyCsvParser;
using TinyCsvParser.Mapping;
using TinyCsvParser.Tokenizer;

namespace InventoryListParser.Parser
{
    public class InventoryCsvParser: IInventoryCsvParser
    {
        private readonly CsvParser<ParsedInventoryLine> _csvParser;

        public InventoryCsvParser()
        {
            CsvParserOptions options = new CsvParserOptions(false, "#", new QuotedStringTokenizer(';'));

            _csvParser = new CsvParser<ParsedInventoryLine>(options, new CsvInventoryMapping());
        }

        public IEnumerable<ParsedInventoryLine> Parse(string csv)
        {
            CsvReaderOptions csvReaderOptions = new CsvReaderOptions(new[] { Environment.NewLine });

            ParallelQuery<CsvMappingResult<ParsedInventoryLine>> result = _csvParser.ReadFromString(csvReaderOptions, csv);

            return TransformMappingResult(result);
        }

        public IEnumerable<ParsedInventoryLine> Parse(Stream csvStream)
        {
            return Parse(csvStream, Encoding.UTF8);
        }

        public IEnumerable<ParsedInventoryLine> Parse(Stream csvStream, Encoding encoding)
        {
            ParallelQuery<CsvMappingResult<ParsedInventoryLine>> result = _csvParser.ReadFromStream(csvStream, encoding);

            return TransformMappingResult(result);
        }

        private IEnumerable<ParsedInventoryLine> TransformMappingResult(
            ParallelQuery<CsvMappingResult<ParsedInventoryLine>> mappingResult)
        {
            return mappingResult
                .Select(r => r.Result)
                .Where(i => i != null);
        }
    }
}