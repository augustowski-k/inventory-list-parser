﻿using TinyCsvParser.Mapping;

namespace InventoryListParser.Parser
{
    public class CsvInventoryMapping: CsvMapping<ParsedInventoryLine>
    {
        public CsvInventoryMapping()
            :base()
        {
            MapProperty(0, i => i.ItemName);
            MapProperty(1, i => i.ItemId);
            MapProperty(2, i => i.CountByWerehouse, new CountByWerehouseTypeConverter());
        }
    }
}
