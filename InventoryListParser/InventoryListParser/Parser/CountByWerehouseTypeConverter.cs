﻿using System;
using System.Collections.Generic;
using TinyCsvParser.TypeConverter;

namespace InventoryListParser.Parser
{
    public class CountByWerehouseTypeConverter : ITypeConverter<Dictionary<string, int>>
    {
        public Type TargetType => typeof(Dictionary<string, int>);

        public bool TryConvert(string value, out Dictionary<string, int> result)
        {
            result = new Dictionary<string, int>();

            if(string.IsNullOrEmpty(value))
            {
                return false; 
            }

            foreach (string werehouseCount in value.Split('|'))
            {
                string[] parts = werehouseCount.Split(',');
                if(parts.Length != 2)
                {
                    return false;
                }

                result.Add(parts[0], int.Parse(parts[1]));
            }
            return true;
        }
    }
}
