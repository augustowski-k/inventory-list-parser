﻿using System.Collections.Generic;

namespace InventoryListParser.Parser
{
    public class ParsedInventoryLine
        {
            public string ItemName { get; set; }

            public string ItemId { get; set; }

            public Dictionary<string, int> CountByWerehouse { get; set; }
        }
}
