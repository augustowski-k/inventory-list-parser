﻿using System.Collections.Generic;
using System.Linq;

namespace InventoryListParser.Models
{
    public class Werehouse
    {
        private List<InventoryItem> _items;

        public Werehouse(string name)
        {
            Id = name;
            _items = new List<InventoryItem>();
        }

        public string Id { get; set; }

        public IEnumerable<InventoryItem> Items => _items.AsReadOnly();

        public IOrderedEnumerable<InventoryItem> ItemsDefaultOrder => _items.OrderBy(i => i.Id);

        public int ItemCount => Items.Aggregate(0, (count, item) => count + item.Count);

        public void AddItem(InventoryItem item)
        {
            InventoryItem existingItem = _items.FirstOrDefault(i => i.Id == item.Id);
            if(existingItem != null)
            {
                existingItem.Count += item.Count;
            }
            else
            {
                _items.Add(item);
            }
        }
    }
}
