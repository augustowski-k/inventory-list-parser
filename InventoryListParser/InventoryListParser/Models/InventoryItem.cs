﻿namespace InventoryListParser.Models
{
    public class InventoryItem
    {
        public InventoryItem(string id, int count)
        {
            Id = id;
            Count = count;
        }

        public string Id { get; set; }

        public int Count { get; set; }
    }
}