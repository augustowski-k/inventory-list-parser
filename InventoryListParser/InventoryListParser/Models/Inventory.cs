﻿using System.Collections.Generic;
using System.Linq;

namespace InventoryListParser.Models
{
    public class Inventory
    {
        private List<Werehouse> _werehouses;

        public Inventory(List<Werehouse> werehouses)
        {
            _werehouses = werehouses;
        }

        public IEnumerable<Werehouse> WerehouseList => _werehouses.AsReadOnly();

        public IOrderedEnumerable<Werehouse> WerehouseListDefaultOrder => _werehouses
            .OrderByDescending(w => w.ItemCount)
            .ThenByDescending(w => w.Id);
    }
}
