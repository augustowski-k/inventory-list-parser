﻿using InventoryListParser.Models;
using InventoryListParser.Parser;
using System.Collections.Generic;
using System.IO;

namespace InventoryListParser
{
    public class InventoryReader: IInventoryReader
    {
        private readonly IInventoryCsvParser _inventoryCsvParser;
        private readonly IInventoryBinder _inventoryBinder;

        public InventoryReader(
            IInventoryCsvParser inventoryCsvParser,
            IInventoryBinder inventoryBinder)
        {
            _inventoryCsvParser = inventoryCsvParser;
            _inventoryBinder = inventoryBinder;
        }

        public Inventory GetInventory(FileInfo inventoryFile)
        {
            using (FileStream inventoryStream = inventoryFile.OpenRead())
            {
                IEnumerable<ParsedInventoryLine> parsedInventoryLines = _inventoryCsvParser.Parse(inventoryStream);

                return _inventoryBinder.Bind(parsedInventoryLines);
            }
        }
        
        public Inventory GetInventory(string inventoryCsv)
        {
            IEnumerable<ParsedInventoryLine> parsedInventoryLines = _inventoryCsvParser.Parse(inventoryCsv);

            return _inventoryBinder.Bind(parsedInventoryLines);
        }
    }
}
