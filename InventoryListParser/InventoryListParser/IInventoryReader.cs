﻿using InventoryListParser.Models;
using System.IO;

namespace InventoryListParser
{
    public interface IInventoryReader
    {
        Inventory GetInventory(FileInfo inventoryFile);

        Inventory GetInventory(string inventoryCsv);
    }
}