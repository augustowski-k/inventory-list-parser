﻿using InventoryListParser.Models;
using InventoryListParser.Parser;
using System.Collections.Generic;

namespace InventoryListParser
{
    public interface IInventoryBinder
    {
        public Inventory Bind(IEnumerable<ParsedInventoryLine> parsedInventoryLines);
    }
}