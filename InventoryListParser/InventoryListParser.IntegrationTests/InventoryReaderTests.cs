﻿using InventoryListParser.Parser;
using NUnit.Framework;
using System.Linq;
using System.Text;

namespace InventoryListParser.Tests
{
    [TestFixture]
    public class InventoryReaderTests
    {
        //second param is just quickfix for test
        [TestCase(new[] {
                "Cherry Hardwood Arched Door - PS;COM-100001;WH-A,5",
                "Cherry Hardwood Arched Door - PS;COM-100001;WH-A,5|WH-B,10",
                "Cherry Hardwood Arched Door - PS;COM-100001;WH-Z,3"
            }, 1)]
        public void GetInventory_MultilineString_Returns(string[] lines, int i)
        {
            StringBuilder stringBuilder = new StringBuilder();
            foreach(string line in lines)
            {
                stringBuilder.AppendLine(line);
            }

            InventoryReader reader = new InventoryReader(new InventoryCsvParser(), new InventoryBinder());
            var result = reader.GetInventory(stringBuilder.ToString());

            Assert.That(result.WerehouseList.Count(), Is.EqualTo(3));
        }
    }
}
