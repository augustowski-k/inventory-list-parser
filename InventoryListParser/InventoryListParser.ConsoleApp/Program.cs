﻿using InventoryListParser.Models;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;

namespace InventoryListParser.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            ServiceCollection services = new ServiceCollection();
            services.AddInventoryListParser();

            var provider = services.BuildServiceProvider();

            Console.WriteLine("Enter path to csv");
            string csvPath = Console.ReadLine();
            FileInfo csv = new FileInfo(csvPath);

            IInventoryReader reader = provider.GetService<IInventoryReader>();
            Inventory inventory = reader.GetInventory(csv);

            WriteOutput(inventory);

            Console.ReadLine();
        }

        private static void WriteOutput(Inventory inventory)
        {
            foreach (var werehouse in inventory.WerehouseListDefaultOrder)
            {
                Console.WriteLine($"{werehouse.Id} (total {werehouse.ItemCount})");
                foreach (var item in werehouse.ItemsDefaultOrder)
                {
                    Console.WriteLine($"{item.Id}: {item.Count}");
                }
                Console.WriteLine();
            }
        }
    }
}
