﻿using InventoryListParser.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;

namespace InventoryListParser.Web.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        private readonly IInventoryReader _inventoryReader;

        public IndexModel(
            ILogger<IndexModel> logger,
            IInventoryReader inventoryReader)
        {
            _logger = logger;
            _inventoryReader = inventoryReader;
        }

        [BindProperty]
        public string InventoryCsv { get; set; }

        public Inventory Inventory { get; set; }

        public void OnGet()
        {

        }

        public void OnPost()
        {
            if (!string.IsNullOrEmpty(InventoryCsv))
            {
                Inventory = _inventoryReader.GetInventory(InventoryCsv);
            }
        }
    }
}
