# Inventory List Parser


To run applications inside you need to have .net core 3.1 SDK installed  
Call `dotnet build` in InventoryListParser directory placed in root of repository  
To run console app then run `dotnet .\InventoryListParser.ConsoleApp\bin\Debug\netcoreapp3.1\InventoryListParser.ConsoleApp.dll"`  
To run web app run `dotnet .\InventoryListParser.Web\bin\Debug\netcoreapp3.1\InventoryListParser.Web.dll` and navigate to url visible in console

All commands need to be run from directory `InventoryListParser` placed in the root of this repo


Using TinyCsvParser library under following license:
https://github.com/bytefish/TinyCsvParser/blob/master/LICENSE